import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import fontawesome from "./plugins/fontawesone";
import VueI18n from "vue-i18n";

Vue.config.productionTip = false;

Vue.use(VueI18n);

const i18n = new VueI18n({
  locale: "es",
  messages: {
    'es': require('../i18n/es.json'),
    'en': require('../i18n/en.json')
  },
});

new Vue({
  router,
  store,
  i18n,
  fontawesome,
  render: (h) => h(App),
}).$mount("#app");
