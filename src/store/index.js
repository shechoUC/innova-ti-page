import Vue from "vue";
import Vuex from "vuex";
import esJson from "../../i18n/es.json";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    finalData: null,
  },
  mutations: {
    SET_DATA(state, datos) {
      state.finalData = datos;
    },
  },
  actions: {
    async getData(languaje) {
      const datos = await esJson;
      console.log('languaje from store: ',languaje);
      this.commit("SET_DATA", datos);
    },
  },
  modules: {},
});
